# CSFS1030 - Course Project

# Author: Jordan Lee

## Set-up

Before starting, create a `.env` file and copy and paste the contents from `.envfile` into it.

Create the database: portfolioDB and tables in MySQL from the info.sql file

Once npm is started the terminal should read: Server started on http://localhost:3001 and MySQL database is connected

Login Credentials are: Email: Jordan@Lee.com & Password: zxcvasdfqwer

Once logged in the top right should provide a dropdown menu to view Submissions, Add User, and Edit Portfolio.
